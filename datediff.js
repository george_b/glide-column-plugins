/* 
  Returns a string representing the time difference between  
  two dates rounded to the unit precision. 
  Test here: onecompiler.com/javascript or jsfiddle.net/

  Parameters:
    d1, d2 - js dates
    units - an array of stings that represent the units. This also controls
      the precision desired. The order should be:
      ['weeks','days','hours','minutes','seconds','milliseconds'].
      Omitting units from the end of the array will omit from the duration.
*/
 
function duration(d1, d2, units, separator) {
  let d = (d1 > d2 ? d1 - d2 : d2 - d1);
  let wks = Math.floor(d / 1000 / 60 / 60 / 24 / 7);
  let days = Math.floor(d / 1000 / 60 / 60 / 24 - wks * 7);
  let hrs = Math.floor(d / 1000 / 60 / 60 - wks * 7 * 24 - days * 24);
  let min = Math.floor(
    d / 1000 / 60 - wks * 7 * 24 * 60 - days * 24 * 60 - hrs * 60
  );
  let sec = Math.floor(
    d / 1000 -
      wks * 7 * 24 * 60 * 60 -
      days * 24 * 60 * 60 -
      hrs * 60 * 60 -
      min * 60
  );
  let milsec = Math.floor(d - wks*7*24*60*60*1000 - days*24*60*60*1000 - hrs*60*60*1000 - min*60*1000 - sec*1000);
  let output = '';
  
  // rounded up array
  let roundUp = [wks,days,hrs,min,sec,milsec];

  if (roundUp[5] > 499) {
    roundUp[4] += 1
    if (roundUp[4] > 60) {
      roundUp[3] +=1;
      roundUp[4] -= 60;
    }
  };
  if (roundUp[4] > 29) {
    roundUp[3] += 1;
    if (roundUp[3] > 60) {
      roundUp[2] += 1;
      roundUp[3] -= 60;
    }
  }
  if (roundUp[3] > 29) {
    roundUp[2] += 1;
    if (roundUp[2] > 24) {
      roundUp[1] += 1;
      roundUp[2] -= 24;
    }
  }
  if (roundUp[2] > 11) {
    roundUp[1] += 1;
    if (roundUp[1] > 7) {
      roundUp[0] += 1;
      roundUp[1] -= 7;
    }
  }

  for (var i = 0; i < units.length; i++) {
    if ((i === 0) && (wks > 0)) {
      // round up if this is the last unit
      if (i + 1 === units.length) {wks = roundUp[i]};
      output += wks + ' ' + units[i];
      // don't add the separator if this is the last unit
      if (i + 1 !== units.length) {output += separator};
    };
    if ((i === 1) && (days > 0)) {
      if (i + 1 === units.length) {days = roundUp[i]};
      output += days + ' ' + units[i];
      if (i + 1 !== units.length) {output += separator};
    };
    if ((i === 2) && (hrs > 0)) {
      if (i + 1 === units.length) {hrs = roundUp[i]};
      output += hrs + ' ' + units[i];
      if (i + 1 !== units.length) {output += separator};
    };
    if ((i === 3) && (min > 0)) {
      if (i + 1 === units.length) {min = roundUp[i]};
      output += min + ' ' + units[i];
      if (i + 1 !== units.length) {output += separator};
    };
    if ((i === 4) && (sec > 0)) {
      if (i + 1 === units.length) {sec = roundUp[i]};
      output += sec + ' ' + units[i];
      if (i + 1 !== units.length) {output += separator};
    };
    if ((i === 5) && (milsec > 0)) {
      output += milsec + ' ' + units[i];
    };
  }
  return output;
}

console.log(duration(new Date(), new Date("2021-10-12"), ['w','d','h','m','s','ms'], ' '))
console.log(duration(new Date("2021-10-12"), new Date(), ['w','d','h','m'], ' '))
